import os
import subprocess
import csv

perf_csv= open('/tmp/'+os.uname()[1]+'.csv','w+')
perf_cmd = ['vmstat -tan' ,'iostat -x','mpstat','swapon -s','uptime','hostname -i','hostname']
perf_dict={}
for i in range(len(perf_cmd)):
        perf_dict[perf_cmd[i]]=subprocess.Popen(perf_cmd[i].split(),stdout=subprocess.PIPE).communicate()[0].strip()
csv_columns_head=perf_dict.keys()
writer =csv.DictWriter(perf_csv,fieldnames=csv_columns_head)
writer.writeheader()
writer.writerow(perf_dict)
perf_csv.close()
